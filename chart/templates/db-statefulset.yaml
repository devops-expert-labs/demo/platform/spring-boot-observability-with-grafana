apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: realworld-db
  labels:
    app.kubernetes.io/name: {{ include "realworld.name" . }}-db
    helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/version: {{ .Chart.AppVersion }}
    app.kubernetes.io/component: db
    app.kubernetes.io/part-of: {{ include "realworld.name" . }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
spec:
  serviceName: realworld-db
  replicas: 1
  minReadySeconds: 10
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "realworld.name" . }}-db
      app.kubernetes.io/instance: {{ .Release.Name }}
      app.kubernetes.io/version: {{ .Chart.AppVersion }}
      app.kubernetes.io/component: db
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "realworld.name" . }}-db
        app.kubernetes.io/instance: {{ .Release.Name }}
        app.kubernetes.io/version: {{ .Chart.AppVersion }}
        app.kubernetes.io/component: db
    spec:
      terminationGracePeriodSeconds: 10
      containers:
        - name: {{ .Chart.Name }}-db
          image: "{{ .Values.db.image.repository }}:{{ .Values.db.image.tag }}"
          imagePullPolicy: {{ .Values.db.image.pullPolicy }}
          ports:
            - name: db
              containerPort: 5432
              protocol: TCP
          lifecycle:
            postStart:
              exec:
                command:
                  - /bin/sh
                  - -ec
                  - |
                    sleep 10 && \
                    PGPASSWORD=$POSTGRES_PASSWORD psql -h localhost -U $POSTGRES_USER -d $POSTGRES_DB \
                      -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";"
          env:
            - name: POSTGRES_DB
              valueFrom:
                secretKeyRef:
                  name: db-credentials
                  key: DB_NAME
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: db-credentials
                  key: DB_USER
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: db-credentials
                  key: DB_PASSWD
          volumeMounts:
            - name: db-data
              mountPath: /var/lib/postgresql/data
              subPath: pgdata
  volumeClaimTemplates:
    - metadata:
        name: db-data
      spec:
        storageClassName: {{ .Values.db.persistence.storageClassName | quote }}
        accessModes:
        {{- range .Values.db.persistence.accessModes }}
          - {{ . | quote }}
        {{- end }}
        resources:
          requests:
            storage: {{ .Values.db.persistence.size | quote }}
