package com.example.realworld.security;

import com.example.realworld.security.jwt.JwtAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Arrays;
import java.util.Collections;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
@EnableWebMvc
public class WebConfig {

    private final JwtAuthenticationFilter jwtAuthenticationFilter;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList(CorsConfiguration.ALL));
        //configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
        configuration.setAllowedHeaders(Collections.singletonList(CorsConfiguration.ALL));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowCredentials(false);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .headers()
                .frameOptions().disable()
                .and()
            .cors().configurationSource(corsConfigurationSource())
                .and()
            .csrf().disable()
            .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
            .authorizeHttpRequests((authz) -> authz
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                .requestMatchers(antMatcher(HttpMethod.GET,"/api/articles")).permitAll()
                .requestMatchers(antMatcher(HttpMethod.GET, "/api/articles/**")).permitAll() // '/api/articles/:slug', '/api/articles/:slug/comments'
                .requestMatchers(antMatcher(HttpMethod.GET,"/api/user")).permitAll()
                .requestMatchers(antMatcher(HttpMethod.POST, "/api/users/**")).permitAll()
                .requestMatchers(antMatcher(HttpMethod.GET, "/api/profiles/**")).permitAll() // '/api/profiles/:username'
                .requestMatchers(antMatcher(HttpMethod.GET, "/api/tags")).permitAll()
                .requestMatchers(antMatcher(HttpMethod.GET, "/actuator/health")).permitAll()
                .requestMatchers(antMatcher(HttpMethod.GET, "/actuator/prometheus")).permitAll()
                // POST '/api/articles', GET '/api/articles/feed', PUT/DELETE '/api/articles/:slug',
                // POST '/api/articles/:slug/favorite', DELETE '/api/articles/:slug/favorite',
                // POST '/api/articles/:slug/comments', DELETE '/api/articles/:slug/comments/:id',
                // POST/DELETE '/api/profiles/:username/follow'
                .anyRequest().authenticated()
            )
            .requestCache().disable()
            .securityContext().disable()
            .sessionManagement().disable()
            .formLogin().disable()
            .exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
        // @formatter:on

        return http.build();
    }

}
