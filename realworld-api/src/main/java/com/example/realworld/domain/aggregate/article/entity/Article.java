package com.example.realworld.domain.aggregate.article.entity;

import com.example.realworld.base.entity.DateEntity;
import com.example.realworld.domain.aggregate.tag.entity.Tag;
import com.example.realworld.domain.aggregate.user.entity.User;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "article")
public class Article extends DateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String slug;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String body;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "article_tag",
            joinColumns = @JoinColumn(name = "article_id", foreignKey = @ForeignKey(name = "article_tag_fk_article_id")),
            inverseJoinColumns = @JoinColumn(name = "tag_id", foreignKey = @ForeignKey(name = "article_tag_fk_tag_id"))
    )
    private List<Tag> tags;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private User author;

    @ManyToMany
    @JoinTable(
            name = "favorite",
            joinColumns = @JoinColumn(name = "article_id", foreignKey = @ForeignKey(name = "favorite_fk_article_id")),
            inverseJoinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "favorite_fk_user_id"))
    )
    private List<User> favoritedBy;

    @OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
    private List<Comment> comments;

    public void setTags(List<Tag> tags) {
        if (CollectionUtils.isEmpty(tags))
            return;
        if (this.getTags() == null) {
            this.tags = new ArrayList<>();
        }
        this.tags.addAll(tags);
    }

    public void changeTitle(String title) {
        this.title = title;
    }

    public void changeSlug(String slug) {
        this.slug = slug;
    }

    public void changeBody(String body) {
        this.body = body;
    }

    public void changeDescription(String description) {
        this.description = description;
    }

}
