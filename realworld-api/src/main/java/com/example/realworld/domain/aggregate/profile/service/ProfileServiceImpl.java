package com.example.realworld.domain.aggregate.profile.service;

import com.example.realworld.domain.aggregate.profile.dto.ProfileResponse;
import com.example.realworld.domain.aggregate.profile.entity.Follow;
import com.example.realworld.domain.aggregate.profile.entity.FollowId;
import com.example.realworld.domain.aggregate.profile.repository.ProfileRepository;
import com.example.realworld.domain.aggregate.user.dto.UserAuth;
import com.example.realworld.domain.aggregate.user.entity.User;
import com.example.realworld.domain.aggregate.user.repository.UserRepository;
import com.example.realworld.exception.CustomException;
import com.example.realworld.exception.Error;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {
    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;

    @Override
    public ProfileResponse getProfile(UserAuth userAuth, String username) {
        Optional<User> foundUser = Optional.ofNullable(userRepository.findByUsername(username).orElseThrow(() -> {
            throw new CustomException(Error.USER_NOT_FOUND);
        }));
        boolean following = false;
        if (userAuth != null) {
            following = profileRepository.findByFolloweeIdAndFollowerId(foundUser.get().getId(), userAuth.getId()).isPresent();
        }
        return convertProfile(foundUser, following);
    }

    @Override
    public ProfileResponse followUser(UserAuth userAuth, String username) {
        Optional<User> followee = Optional.ofNullable(userRepository.findByUsername(username).orElseThrow(() -> {
            throw new CustomException(Error.USER_NOT_FOUND);
        }));
        Optional<User> follower = userRepository.findById(userAuth.getId());
        profileRepository.findByFolloweeIdAndFollowerId(followee.get().getId(), follower.get().getId()).ifPresent(follow -> {
            throw new CustomException(Error.ALREADY_FOLLOW);
        });
        Follow follow = Follow.builder()
                .followId(FollowId.builder().followeeId(followee.get().getId()).followerId(follower.get().getId()).build())
                .followee(followee.get())
                .follower(follower.get())
                .build();
        Boolean following = profileRepository.save(follow).getFollower().getId().equals(follower.get().getId());
        return convertProfile(followee, following);
    }

    @Override
    public ProfileResponse unfollowUser(UserAuth userAuth, String username) {
        Optional<User> followee = Optional.ofNullable(userRepository.findByUsername(username).orElseThrow(() -> {
            throw new CustomException(Error.USER_NOT_FOUND);
        }));
        Optional<User> follower = userRepository.findById(userAuth.getId());
        Follow follow = profileRepository.findByFolloweeIdAndFollowerId(followee.get().getId(), follower.get().getId()).orElseThrow(() -> {
            throw new CustomException(Error.ALREADY_UNFOLLOW);
        });
        profileRepository.delete(follow);
        return convertProfile(followee, false);
    }

    private ProfileResponse convertProfile(Optional<User> user, Boolean following) {
        return ProfileResponse.builder().
                username(user.get().getUsername()).
                bio(user.get().getBio()).
                image(user.get().getImage()).
                following(following).build();
    }
}
