package com.example.realworld.config;

import io.micrometer.core.instrument.Clock;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exemplars.DefaultExemplarSampler;
import io.prometheus.client.exemplars.tracer.otel_agent.OpenTelemetryAgentSpanContextSupplier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PrometheusExemplarConfiguration {

    @Bean
    public PrometheusMeterRegistry prometheusMeterRegistryWithExemplar(PrometheusConfig prometheusConfig, CollectorRegistry collectorRegistry, Clock clock) {
        return new PrometheusMeterRegistry(prometheusConfig, collectorRegistry, clock, new DefaultExemplarSampler(new OpenTelemetryAgentSpanContextSupplier() {
            @Override
            public String getTraceId() {
                if (!super.isSampled()) {
                    return null;
                }
                return super.getTraceId();
            }
        }));
    }

}
