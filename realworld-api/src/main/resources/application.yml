spring:
  profiles:
    default: local

  jpa:
    database-platform: org.hibernate.dialect.PostgreSQLDialect
    hibernate:
      ddl-auto: validate
    open-in-view: true
    properties:
      hibernate:
        format_sql: true
        default_schema: public
    show-sql: false

server:
  compression:
    enabled: true
management:
  endpoints:
    enabled-by-default: false
    jmx:
      exposure:
        exclude: '*'
    web:
      exposure:
        include: health, prometheus
  metrics:
    tags:
      application: 'realworld-api'
    # Exemplar metrics
    distribution:
      percentiles-histogram:
        http.server.requests: true
  endpoint:
    health:
      enabled: true
    prometheus:
      enabled: true


---

# ==================================================================================================================== #
# Local profile configuration
# ==================================================================================================================== #
spring:
  config:
    activate:
      on-profile: local

  # Datasource (DataSourceAutoConfiguration & DataSourceProperties)
  datasource:
    driver-class-name: net.sf.log4jdbc.sql.jdbcapi.DriverSpy
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:log4jdbc:postgresql://${DB_HOST:localhost}:${DB_PORT:5432}/${DB_NAME:local_db}?ssl=false
    username: ${DB_USER:dev}
    password: ${DB_PASSWD:dev123}
    hikari:
      data-source-properties:
        cachePrepStmts: true
        prepStmtCacheSize: 250
        prepStmtCacheSqlLimit: 2048
        useServerPrepStmts: true
        useLocalSessionState: true
        rewriteBatchedStatements: true
        cacheResultSetMetadata: true
        cacheServerConfiguration: true
        elideSetAutoCommits: true
        maintainTimeStats: false

  # Liquibase
  liquibase:
    change-log: classpath:/db/changelog/db.changelog-master.yaml
    enabled: true

server:
  compression:
    enabled: false

# Logging
logging:
  level:
    root: DEBUG
    org.springframework: WARN
    org.springframework.aop.interceptor: INFO
    org.springframework.beans: INFO
    org.springframework.boot: DEBUG
    org.springframework.boot.autoconfigure: INFO
    org.springframework.boot.liquibase: INFO
    org.springframework.web: INFO
    org.springframework.web.context: WARN
    #org.hibernate.resource: INFO
    org.hibernate: INFO
    jdbc.sqlonly: DEBUG
    jdbc.sqltiming: DEBUG
    jdbc.audit: OFF
    jdbc.resultset: OFF
    jdbc.resultsettable: DEBUG
    jdbc.connection: OFF
    com.zaxxer.hikari.pool: INFO
    log4jdbc.debug: OFF
    liquibase: INFO
    com.example.realworld: DEBUG
  pattern:
    console: "%-42(%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread]) %highlight(%-6level) %-50(%cyan(%logger{15})) - %msg %n"

# JWT
jwt:
  token:
    secretKey: realworldPostGreSQL0132474654564564213131d31vfxvjfijkjdks
    validityInMillis: 3600000 # 1h

---

# ==================================================================================================================== #
# Production profile configuration
# ==================================================================================================================== #
spring:
  config:
    activate:
      on-profile: prod

  # Datasource (DataSourceAutoConfiguration & DataSourceProperties)
  datasource:
    driver-class-name: org.postgresql.Driver
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:postgresql://${DB_HOST}:${DB_PORT}/${DB_NAME}?ssl=false
    username: ${DB_USER}
    password: ${DB_PASSWD}
    hikari:
      data-source-properties:
        cachePrepStmts: true
        prepStmtCacheSize: 250
        prepStmtCacheSqlLimit: 2048
        useServerPrepStmts: true
        useLocalSessionState: true
        rewriteBatchedStatements: true
        cacheResultSetMetadata: true
        cacheServerConfiguration: true
        elideSetAutoCommits: true
        maintainTimeStats: false

  jpa:
    hibernate:
      ddl-auto: validate

  # Liquibase
  liquibase:
    change-log: classpath:/db/changelog/db.changelog-master.yaml
    enabled: true

# Logging
logging:
  level:
    root: INFO
    org.springframework: WARN
    org.springframework.aop.interceptor: INFO
    org.springframework.beans: INFO
    org.springframework.boot: INFO
    org.springframework.boot.autoconfigure: INFO
    org.springframework.boot.liquibase: INFO
    org.springframework.web: INFO
    org.springframework.web.context: WARN
    jdbc.sqlonly: INFO
    jdbc.sqltiming: INFO
    jdbc.audit: OFF
    jdbc.resultset: OFF
    jdbc.resultsettable: OFF
    jdbc.connection: OFF
    com.zaxxer.hikari.pool: INFO
    log4jdbc.debug: OFF
    liquibase: INFO
    org.thymeleaf: INFO
    com.example.realworld: INFO
  pattern:
    console: "%-42(%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread]) %-6level - %-50(%logger{15}) - %msg trace_id=%X{trace_id} span_id=%X{span_id} trace_flags=%X{trace_flags} %n"

# JWT
jwt:
  token:
    secretKey: realworldPostGreSQL0132474654564564213131d31vfxvjfijkjdks
    validityInMillis: 3600000 # 1h
