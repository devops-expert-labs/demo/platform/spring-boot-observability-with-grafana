# RealWorld Spring Boot

![RealWorld Spring Boot App](logo.png)

> 이 코드베이스는 완전히 동일한 **Medium.com** 클론(Conduit이라고 함)이 서로 다른 프런트엔드(React, Angular 등)와 백엔드(Node, Django 등)를 사용하여 구축하는 방법을 제공하는 [RealWorld](https://github.com/gothinkster/realworld)의 백엔드 프로젝트 중 하나인 [real-world-springboot-java](https://github.com/kkminseok/real-world-springboot-vue.js)를 기반으로 작성하였습니다. RealWorld는 프런트엔드와 백엔드 모두 동일한 API 사양을 준수하므로 조합하여 일치시킬 수 있습니다. 😮😎

## 기술 스택

* Java 17
* Gradle 7.5 이상
* Spring Boot
* Spring Framework
* Spring Data JPA
* Spring Security
* Java JWT (JJWT)
* Liquibase
* Lombok
* JUnit5 및 Mockito
* PostgreSQL

## 로컬 개발 환경 구성

### PostgreSQL

Homebrew를 사용하여 PostgreSQL을 설치하고 실행합니다.

```bash
brew install postgresql@15
brew services start postgresql@15
brew services list
```

환경 변수를 설정합니다.

```bash
echo 'export PATH="/opt/homebrew/opt/postgresql@15/bin:$PATH"' >> ~/.zshrc
source ~/.zshrc
```

설치한 PostgreSQL 버전을 확인합니다.

```bash
$ postgres -V
postgres (PostgreSQL) 15.2 (Homebrew)
```

PostgreSQL에 접속하여 사용자 및 데이터베이스를 생성하고, 권한을 부여합니다.

```bash
$ psql postgres
psql (15.2 (Homebrew))
Type "help" for help.

postgres=# CREATE USER dev WITH ENCRYPTED PASSWORD 'dev123';
postgres=# CREATE DATABASE local_db OWNER dev ENCODING 'UTF8';
postgres=# GRANT ALL PRIVILEGES ON DATABASE local_db TO dev;
postgres=# \q
```

Homebrew를 사용하여 pgAdmin4를 설치합니다.

```bash
brew install --cask pgadmin4
```

### Gradle

Homebrew를 사용하여 Gradle을 설치합니다.

```bash
brew install gradle
```

설치한 Gradle 버전을 확인합니다.

```bash
$ gradle -v

------------------------------------------------------------
Gradle 8.0.2
------------------------------------------------------------
```

필요하면 Gradle Wrapper를 사용하여 Gradle 버전을 업데이트합니다.

```bash
./gradlew wrapper --gradle-version=8.0.2 --distribution-type=bin
```

### 로컬 환경에서 애플리케이션 실행

```bash
./gradlew bootRun
```

### 배포 환경에서 실행

```bash
./gradlew assemble
```

```bash
SPRING_PROFILES_ACTIVE=prod \
DB_HOST=<Database-Host> \
DB_PORT=<Database-Port> \
DB_NAME=<Database-Name> \
DB_USER=<Database-User> \
DB_PASSWD=<Database-Password> \
java -jar build/libs/realworld-0.0.1-SNAPSHOT.jar
```

## 테스트

### 단위 테스트

PostgreSQL에 접속하여 테스트용 데이터베이스를 생성하고, 권한을 부여합니다.

```bash
$ psql postgres
psql (15.2 (Homebrew))
Type "help" for help.

postgres=# CREATE DATABASE test_db OWNER dev ENCODING 'UTF8';
postgres=# GRANT ALL PRIVILEGES ON DATABASE test_db TO dev;
postgres=# \q
```

단위 테스트를 실행합니다.

```bash
./gradlew clean test
```
