# Spring Boot Observability with Grafana

이 프로젝트에는 인포그랩의 [DevOps Expert Labs](https://insight.infograb.net/devops/expert-labs)에서 **Grafana를 이용한 Spring Boot Observability 구현** 과정의 실습에 필요한 리소스가 포함되어 있습니다.

* Spring Boot를 사용하여 구현된 RealWorld 백엔드 애플리케이션의 소스 코드
* RealWorld Spring Boot 애플리케이션을 배포하는 데 필요한 Helm 차트
* Spring Boot 애플리케이션의 Grafana 옵저버빌리티 대시보드 구성에 필요한 JSON 파일
* Spring Boot 애플리케이션에 부하를 생성하는 데 필요한 스크립트
